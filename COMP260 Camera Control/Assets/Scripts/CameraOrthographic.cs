﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOrthographic : MonoBehaviour
{
    public Transform[] rocks;

    public float boundingBoxPadding = 2f;
    public float mininumOrthographicSize = 8f;
    public float zoomSpeed = 20f;

    public Camera mainCamera;


    public void Awake()
    {
        mainCamera = GetComponent<Camera>();
        mainCamera.orthographic = true;

        //rocks = GameObject.FindGameObjectsWithTag("Rock");
        //foreach(GameObject )

    }

    public void LateUpdate()
    {
        Rect boundingBox = CalculateTargetsBoundingBox();
        transform.position = CalculateCameraPosition(boundingBox);
        mainCamera.orthographicSize = CalculateOrthographicSize(boundingBox);
    }

    ///<summary>
    ///Calculate a bounding box that contains all targets
    /// </summary>
    /// <returns>A rect containing all the targets.</returns>
    Rect CalculateTargetsBoundingBox()
    {
        //Bounds are infinite
        float minX = Mathf.Infinity;
        float maxX = Mathf.NegativeInfinity;
        float minY = Mathf.Infinity;
        float maxY = Mathf.NegativeInfinity;

        //For each rock in rocks 

        //Gets ALL rocks


        //Solutions:
        //if (rock count =2, nothing, else if <2 + else if >2
        //OR
        //Raycast to centre, find distance to 2 closest rocks
        foreach (Transform rock in rocks)
        {
            Vector3 position = rock.position;

            minX = Mathf.Min(minX, position.x);
            maxX = Mathf.Max(maxX, position.x);
            minY = Mathf.Min(minY, position.y);
            maxY = Mathf.Max(maxY, position.y);
        }

        return Rect.MinMaxRect( minX - boundingBoxPadding, 
                                maxY + boundingBoxPadding, 
                                maxX + boundingBoxPadding, 
                                minY - boundingBoxPadding);

    }

    /// <summary>
    /// Calculates a camera position given the a bounding box containing all the targets.
    /// </summary>
    /// <param name="boundingBox">A Rect bounding box containg all targets.</param>
    /// <returns>A Vector3 in the center of the bounding box.</returns>
    Vector3 CalculateCameraPosition(Rect boundingBox)
    {
        Vector2 boundingBoxCenter = boundingBox.center;
        return new Vector3(boundingBoxCenter.x, boundingBoxCenter.y, mainCamera.transform.position.z);
    }

    /// <summary>
    /// Calculates a new orthographic size for the camera based on the target bounding box -> float
    /// </summary>
    /// <param name="boundingBox">A Rect bounding box containg all targets.</param>
    /// <returns>A float for the orthographic size.</returns>
    float CalculateOrthographicSize(Rect boundingBox)
    {
        float orthographicSize = mainCamera.orthographicSize;
        Vector3 topRight = new Vector3(boundingBox.x + boundingBox.width, boundingBox.y, 0f);
        Vector3 topRightAsViewport = mainCamera.WorldToViewportPoint(topRight);

        if (topRightAsViewport.x >= topRightAsViewport.y)
        {
            orthographicSize = Mathf.Abs(boundingBox.width) / mainCamera.aspect / 2f;
        }
        else
        {
            orthographicSize = Mathf.Abs(boundingBox.height) / 2f;
        }

        return Mathf.Clamp(Mathf.Lerp(mainCamera.orthographicSize, orthographicSize, Time.deltaTime * zoomSpeed)
                           , mininumOrthographicSize, Mathf.Infinity);
    }
}

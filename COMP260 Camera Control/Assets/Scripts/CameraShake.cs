﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{

    public float amplitude = 0.1f;
    public float frequency = 10f;
    public float duration = 0.5f;

    private float timer = 0f;
    private Vector3 direction = Vector3.zero;

    // Update is called once per frame
    void Update()
    {
        if(timer > 0)
        {
            timer -= Time.deltaTime;

            //If timer is still > 0
            if(timer > 0)
            {
                //Shake the camera
                float t = (duration - timer) * frequency;
                float a = amplitude * Mathf.Sin(t * Mathf.PI * 2);
                transform.localPosition = direction * a;
                Debug.Log("PositionMoved");
            }
            else
            {
                //reset to zero offset
                transform.localPosition = Vector3.zero;
            }
        }
    }

    public void Shake(Vector3 dir)
    {
        //reset timer
        timer = duration;
        direction = dir.normalized;
    }
}

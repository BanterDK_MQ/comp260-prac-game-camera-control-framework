﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowWindow : MonoBehaviour
{
    public Transform target;
    public Vector3 zOffset = new Vector3(0, 0, -10);
    public Rect window = new Rect(0, 0, 0, 1);

    private void Update()
    {
        //get target pos
        Vector3 targetPos = target.position;
        //Convert to viewport co-ords
        Vector2 targetViewPos = Camera.main.WorldToViewportPoint(targetPos);

        //clamp -> closest possible point
        Vector2 goalViewPos = window.Clamp(targetViewPos);

        //Convert back to world co-ords
        Vector3 goalPos = Camera.main.ViewportToWorldPoint(goalViewPos);

        //Convert both to local
        targetPos = transform.InverseTransformPoint(targetPos);
        goalPos = transform.InverseTransformPoint(goalPos);

        //Compute camera movement -> xy plane
        Vector3 move = targetPos - goalPos;
        move.z = 0;

        transform.Translate(move);
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        for (int i = 0; i < 4; i++)
        {
            Vector3 f = Camera.main.ViewportToWorldPoint(window.Corner(i)) - zOffset;
            Vector3 t = Camera.main.ViewportToWorldPoint(window.Corner(i+1)) - zOffset;

            Gizmos.DrawLine(f, t);

        }
    }



}

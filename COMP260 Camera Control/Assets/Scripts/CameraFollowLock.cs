﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowLock : MonoBehaviour
{

    public float lerpFactor = 0.5f;
    public float maxSpeed;
    public float maxOffset;
    public Transform target;
    public Vector3 zOffset = new Vector3(0, 0, -10);

 
    private Rigidbody2D targetRb;
    private Vector3 oldPos;
    private Vector3 xyOffset = Vector3.zero;

    void Start()
    {
        targetRb = target.GetComponent<Rigidbody2D>();
        
        //Old2
        //oldPos = target.position;
    }

    // Update is called once per frame
    void Update()
    {
        //Compute velocity
        Vector3 vel = targetRb.velocity;

        //move the camera in front of the target
        //proportional to its velocity
        xyOffset =  Vector3.Lerp(xyOffset, vel * maxOffset / maxSpeed,
                    1 - Mathf.Pow(1 - lerpFactor, Time.deltaTime));
        transform.position = target.position + xyOffset + zOffset;





        //OLD
        //transform.position = target.position + zOffset;
    }
}
